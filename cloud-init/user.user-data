#cloud-config
packages:
  - git
  - python3
  - python3-pip
  - nginx
  - libsystemd-dev

write_files:
  - encoding: utf-8
    content: |
      [Unit]
      Description=The halloween control backend
      After=multi-user.target

      [Service]
      Type=idle
      WorkingDirectory=/usr/halloween
      ExecStart=/usr/bin/python3 /usr/halloween/control/control_server.py

      [Install]
      WantedBy=multi-user.target
    owner: root:root
    permissions: '0644'
    path: /etc/systemd/system/halloween.service
  - encoding: utf-8
    content: |
      server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;
        index index.html;
        server_name halloween-test;

        location /halloween/ {
          proxy_pass http://localhost:8000/;
          proxy_buffering on;
        }

        location / {
          try_files $uri $uri/ =404;
        }
      }
    owner: root:root
    permissions: '0644'
    path: /etc/nginx/sites-available/halloween

runcmd:
  - [ mkdir, -p, /usr/halloween/control ]
  - [ git, clone, 'https://gitlab.com/franks-reich-halloween/control.git', /usr/halloween/control ]
  - [ mkdir, -p, /usr/halloween/control/images/puppets ]
  - [ mkdir, -p, /usr/halloween/control/data ]
  - [ mkdir, -p, /usr/halloween/control/tmp ]
  - [ git, clone, 'https://gitlab.com/franks-reich-halloween/gpio_mock.git', /usr/halloween/control/tmp ]
  - [ cp, -r, /usr/halloween/control/tmp/RPi, /usr/halloween/control ]
  - [ rm, -rf, /usr/halloween/control/tmp ]
  - [ pip3, install, -r, /usr/halloween/control/requirements.txt ]
  - [ systemctl, daemon-reload ]
  - [ systemctl, enable, halloween ]
  - [ systemctl, start, --no-block, halloween ]
  - [ rm, /etc/nginx/sites-enabled/default ]
  - [ ln, -s, /etc/nginx/sites-available/halloween, /etc/nginx/sites-enabled ]
  - [ systemctl, restart, nginx ]
  - [ mkdir, -p, /var/www/html/tmp ]
  - [ rm, /var/www/html/index.nginx-debian.html ]
  - [ git, clone, 'https://gitlab.com/franks-reich-halloween/control-frontend.git', /var/www/html/tmp ]
  - cp -r /var/www/html/tmp/build/* /var/www/html/
  - [ rm, -r, /var/www/html/tmp ]

