# Setup Tools for Halloween Applications

Contains the following tools:
 - Cloud Init script for setup of a lxd container for testing of the halloween application

## Cloud Init Script

The cloud init script was tested with lxd linux containers. It installs the required packages
using apt and pip. It sets up nginx and creates a systemd unit file to control the backend
service.
It pulls the code for the backend and frontend from GitLab and starts the backend and nginx.
To use the cloud init script, follow these steps:
 - Create a new profile for lxd container by copying the default profile:
   
   lxc profile copy default halloween-test

 - Add the contents cloud init yaml script user.user-data to the user data profile attribute

   lxc profile set user.user-data - < user.user-data

 - Create a new container using the newly created profile halloween-test

   lxc launch ubuntu: halloween-test -p halloween-test

